import { controls } from '../../constants/controls';

// data fight
const state = {
  firstFighterBlock: false,
  secondFighterBlock: false,
  firstFighterSuperKick: [],
  secondFighterSuperKick: [],
  firstFighterSuperKickTime: 0,
  secondFighterSuperKickTime: 0
};

export async function fight(firstFighter, secondFighter) {
  const controller = document;

  // kick
  const kick = (e) => {
    if (e.code === controls.PlayerOneAttack && !state.firstFighterBlock) {
      const damageFunction = getDamage(getHitPower(firstFighter), getBlockPower(secondFighter));
      if (damageFunction > 0) {
        secondFighter.health = secondFighter.health - damageFunction;
      }
      console.log('attac 1');
    }

    if (e.code === controls.PlayerTwoAttack && !state.secondFighterBlock) {
      const damageFunction = getDamage(getHitPower(secondFighter), getBlockPower(firstFighter));
      if (damageFunction > 0) {
        firstFighter.health = firstFighter.health - damageFunction;
      }

      console.log('attac 2');
    }

    console.log(`FirstFighter health = ${firstFighter.health}`);
    console.log(`secondFighter health = ${secondFighter.health}`);
  };

  const holdTheBlock = (e) => {
    if (e.code === controls.PlayerOneBlock && !state.firstFighterBlock) {
      state.firstFighterBlock = true;
      console.log('block1');
    }

    if (e.code === controls.PlayerTwoBlock && !state.secondFighterBlock) {
      state.secondFighterBlock = true;
      console.log('block2');
    }
  };

  const withoutTheBlock = (e) => {
    if (e.code === controls.PlayerOneBlock) {
      state.firstFighterBlock = false;
      console.log('not block1');
    }

    if (e.code === controls.PlayerTwoBlock) {
      state.secondFighterBlock = false;
      console.log('not block2');
    }
  };

  const superKick = (e) => {
    superKickFightKeyDown(e, firstFighter, secondFighter);
  };

  const superKickUp = (e) => {
    superKickFightKeyUp(e, firstFighter, secondFighter);
  };

  controller.addEventListener('keyup', kick);
  controller.addEventListener('keydown', holdTheBlock);
  controller.addEventListener('keyup', withoutTheBlock);
  controller.addEventListener('keydown', superKick);
  controller.addEventListener('keyup', superKickUp);

  return new Promise((resolve, reject) => {
    let intervalFight = setInterval(() => {
      if (secondFighter.health <= 0) {
        clearInterval(intervalFight);
        resolve(firstFighter);
      }

      if (firstFighter.health <= 0) {
        clearInterval(intervalFight);
        resolve(secondFighter);
      }

      timeToSuperKick();

    }, 100);
  });
}

function superKickFightKeyDown(e, firstFighter, secondFighter) {
  if (controls.PlayerOneCriticalHitCombination.includes(e.code)) {
    if (!state.firstFighterSuperKick.includes(e.code)) {
      state.firstFighterSuperKick.push(e.code);
    }

    if (state.firstFighterSuperKick.length === 3 && state.firstFighterSuperKickTime <= 0 && state.firstFighterBlock === false) {
      secondFighter.health = secondFighter.health - 2 * firstFighter.attack;
      state.firstFighterSuperKickTime = 10 * 1000;
    }
  }

  if (controls.PlayerTwoCriticalHitCombination.includes(e.code)) {
    if (!state.secondFighterSuperKick.includes(e.code)) {
      state.secondFighterSuperKick.push(e.code);
    }

    if (state.secondFighterSuperKick.length === 3 && state.secondFighterSuperKickTime <= 0 && state.secondFighterBlock === false) {
      firstFighter.health = firstFighter.health - 2 * secondFighter.attack;
      state.secondFighterSuperKickTime = 10 * 1000;
    }
  }
}

function superKickFightKeyUp(e, firstFighter, secondFighter) {
  if (controls.PlayerOneCriticalHitCombination.includes(e.code)) {
    if (state.firstFighterSuperKick.includes(e.code)) {
      const position = state.firstFighterSuperKick.indexOf(e.code);
      state.firstFighterSuperKick = state.firstFighterSuperKick.splice(position, 1);
    }
  }

  if (controls.PlayerTwoCriticalHitCombination.includes(e.code)) {
    if (state.secondFighterSuperKick.includes(e.code)) {
      const position = state.secondFighterSuperKick.indexOf(e.code);
      state.secondFighterSuperKick = state.secondFighterSuperKick.splice(position, 1);
    }
  }
}

function timeToSuperKick() {
  if (state.firstFighterSuperKickTime > 0) {
    state.firstFighterSuperKickTime -= 100;
  } else if (state.firstFighterSuperKickTime < 0) {
    state.firstFighterSuperKickTime = 0;
  }

  if (state.secondFighterSuperKickTime > 0) {
    state.secondFighterSuperKickTime -= 100;
  } else if (state.secondFighterSuperKickTime < 0) {
    state.secondFighterSuperKickTime = 0;
  }
}

export function getDamage(attacker, defender) {
  return attacker - defender;
}

export function getHitPower(attack) {
  //return hit power
  return attack.attack * randomNumber(1, 2);
}

export function getBlockPower(defense) {
  // return block power
  if (state.firstFighterBlock || state.secondFighterBlock) {
    return defense.defense * randomNumber(1, 2);
  }
  return 0;
}

function randomNumber(from, to) {
  return Math.random() * (to - from) + from;
}