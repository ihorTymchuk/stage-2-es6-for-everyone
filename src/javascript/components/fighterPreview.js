import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';

export function createFighterPreview(fighter, position) {
  // todo: show fighter info (image, name, health, etc.)

  // Data fight (image, name, health, etc.)
  const newInfo = { ...fighter };

  // css style
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const scaleFighterClassName = position === 'right' ? 'fighters___right-fighter' : '';

  // create fighter image and details info
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} `
  });

  const fighterInfoDiv = createElement({
    tagName: 'div',
    className: `fighters___info-text`
  });

  const image = createElement({
    tagName: 'img',
    className: `${scaleFighterClassName}`,
    attributes: { src: `${newInfo.source}` }
  });

  fighterInfoDiv.innerText = `${newInfo.name} \n health: ${newInfo.health} \n attack: ${newInfo.attack} \n  defense: ${newInfo.defense}`;

  // Add element in DOM
  if (newInfo.name !== undefined) {
    fighterElement.append(image, fighterInfoDiv);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
