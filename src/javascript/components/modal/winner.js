import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const showWinnerLayer = createElement({
    tagName: 'div',
    className: 'modal-layer',
  })

  const rootWinner = createElement({
    tagName: 'div',
    className: 'modal-root',
  })

  const imgWinner = createElement({
    tagName: 'img',
    attributes: {
      src: `${fighter.source}`
    }
  })

  const headerWinner = createElement({
    tagName: 'div',
    className: 'modal-header',
  })

  const hiddenWinner = createElement({
    tagName: 'button',
    className: 'close-btn'
  })


  headerWinner.innerText = `${fighter.name}`
  hiddenWinner.innerText = `close`
  rootWinner.append(headerWinner, imgWinner, hiddenWinner);
  showWinnerLayer.append(rootWinner);

  return showWinnerLayer
}
